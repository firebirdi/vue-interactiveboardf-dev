import * as types from '../types.js';

const state = {
  blocks: [
    {
      top: 1,
      left: 1,
      width: 300,
      height: 100,
      title: 'Title 1',
      zIndex: 1,
      show: true
    },
    {
      top: 400,
      left: 50,
      width: 300,
      height: 100,
      title: 'Title 2',
      zIndex: 2,
      show: true
    },
    {
      top: 150,
      left: 350,
      width: 300,
      height: 100,
      title: 'Title 3',
      zIndex: 3,
      show: true
    },
    {
      top: 500,
      left: 400,
      width: 300,
      height: 100,
      title: 'Title 4',
      zIndex: 4,
      show: true
    },
    {
      top: 350,
      left: 700,
      width: 300,
      height: 100,
      title: 'Title 5',
      zIndex: 5,
      show: true
    }
  ],
  deletedIds: []
};

const getters = {
  [types.INTERACTIVE_BOARD_BLOCKS]: s => {
    return s.blocks;
  },
  [types.INTERACTIVE_BOARD_DELETED_IDS]: s => {
    return s.deletedIds;
  }
};

const mutations = {
  [types.INTERACTIVE_BOARD_BLOCK_UPDATE]: (s, payload) => {
    const {id, params} = payload;
    Object.assign(s.blocks[id], params);
  },
  [types.INTERACTIVE_BOARD_BLOCKS_SET]: (s, blocks) => {
    s.blocks = blocks;
  },
  [types.INTERACTIVE_BOARD_DELETED_IDS_SET]: (s, newDeletedIds) => {
    s.deletedIds = newDeletedIds;
  }
};

const actions = {
  [types.INTERACTIVE_BOARD_SAVE]: () => {
    localStorage.setItem('blocks', JSON.stringify(state.blocks));
    localStorage.setItem('deleteIds', JSON.stringify(state.deletedIds));
  },
  [types.INTERACTIVE_BOARD_LOAD]: ({commit}) => {
    const blocks = JSON.parse(localStorage.getItem('blocks'));

    if (blocks.length) {
      commit(types.INTERACTIVE_BOARD_BLOCKS_SET, blocks);

      const newDeletedIds = JSON.parse(localStorage.getItem('deleteIds'));
      commit(types.INTERACTIVE_BOARD_DELETED_IDS_SET, newDeletedIds);
    }
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
